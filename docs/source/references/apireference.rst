.. _apireference:

=============
API reference
=============

.. toctree::

    api/run.rst
    api/configuration.rst
    api/datastructures.rst
    api/detectors.rst
    api/detectorproperties.rst
    api/pipelines.rst
    api/exposure.rst
    api/observation.rst
    api/calibration.rst
    api/inputs.rst
    api/outputs.rst
    api/notebook.rst
    api/util.rst
